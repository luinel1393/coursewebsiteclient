import React from "react";
import LogoWhite from "../../../../assets/img/png/logo-white.png";
import SocialLink from "../../SocialLinks";

import "./MyInfo.scss";

export default function MyInfo() {
  return (
    <div className="my-info">
      <img src={LogoWhite} alt="Logo" />
      <h4>
        Entra en el mund del desarrollo web, disfruta creando proyectos de todo
        tipo, deja que tu imaginación fluya y crea verdaderas maravillas.
      </h4>
      <SocialLink />
    </div>
  );
}
