import React from "react";
import { Row, Col, Card, Avatar } from "antd";
import AvatarPersona from "../../../assets/img/png/avatar-user.png";

import "./ReviewsCourses.scss";

export default function ReviewsCourses() {
  return (
    <Row className="reviews-courses">
      <Row>
        <Col lg={4} />
        <Col lg={16} className="reviews-courses__title">
          <h2>
            Forma parte de los +35 mil estudiantes que están aprendiendo con mis
            cursos
          </h2>
        </Col>
        <Col lg={4} />
      </Row>
      <Row>
        <Col lg={4} />
        <Col lg={16}>
          <Row className="row-cards">
            <Col md={8}>
              <CardReview
                name="Luis García"
                subtitle="Alumno de Udemy"
                avatar={AvatarPersona}
                review="Un curso excelente, lo recomiendo a todo el mundo."
              />
            </Col>
            <Col md={8}>
              <CardReview
                name="David Pérez"
                subtitle="Alumno de Udemy"
                avatar={AvatarPersona}
                review="Al principio no entendía pero me ayudaron y después todo fue de maravilla."
              />
            </Col>
            <Col md={8}>
              <CardReview
                name="Valentina Rubio"
                subtitle="Alumno de Udemy"
                avatar={AvatarPersona}
                review="He mejorado un montón mis habilidades"
              />
            </Col>
          </Row>
          <Row className="row-cards">
            <Col md={8}>
              <CardReview
                name="Nicole Smith"
                subtitle="Alumno de Udemy"
                avatar={AvatarPersona}
                review="Ahora estoy lista para conseguir nuevos empleos."
              />
            </Col>
            <Col md={8}>
              <CardReview
                name="Francisco Sánchez"
                subtitle="Alumno de Udemy"
                avatar={AvatarPersona}
                review="El curso de React Native es demasiado brutal. El mejor que he realizado."
              />
            </Col>
            <Col md={8}>
              <CardReview
                name="Julio Iglesias"
                subtitle="Alumno de Udemy"
                avatar={AvatarPersona}
                review="Soy desarollador de Front y la verdad que aprendí muchos nuevos trucos de CSS."
              />
            </Col>
          </Row>
        </Col>
        <Col lg={4} />
      </Row>
    </Row>
  );
}

function CardReview(props) {
  const { name, subtitle, avatar, review } = props;
  const { Meta } = Card;

  return (
    <Card className="reviews-courses__card">
      <p>{review}</p>
      <Meta
        avatar={<Avatar src={avatar} />}
        title={name}
        description={subtitle}
      />
    </Card>
  );
}
