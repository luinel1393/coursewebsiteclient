## Courses Website

Es una aplicación para ofrecer la venta de cursos online. También cuenta con un Blog donde se muestran noticias recientes relacionadas con el mundo de la informática.

Cuenta con un panel de administración donde se podrá gestionar el contenido de la página.

Fue desarrollada siguiendo el curso de Agustín Navarro Galdon disponible en la plataforma de Udemy en:
[Web Personal MERN Full Stack: MongoDB, Express, React y Node](https://www.udemy.com/course/web-personal-mern-full-stack-mongodb-express-react-node/)

La aplicación en conjunto está conformada por un lado Cliente y un lado servidor.

### Tecnologías

Desarrollo Full Stack:

- MongoDB
- Express
- React
- Node
- Yarn para el manejo de dependencias

### Cliente

La carpeta donde está contenido el presente "README" corresponde al lado cliente de la aplicación y para su correcto funcionamiento ya debe estar instalado y desplegado el lado servidor ya que está apliación consume una API de la otra.

El puerto por defecto donde corre la aplicación es el 3000.

Instalar las depedencias e iniciar el servicio de la app.

```sh
$ cd cliente
$ yarn
$ yarn dev
```

#### Panel de administración

Para ingresar al panel de administración basta con colocar "/admin" en el dominio principal de la página. Ejemplo:
[http://localhost:3000/admin](http://localhost:3000/admin)

Un usuario por defecto con el que se puede iniciar sesión es:
Email: admin.course@yopmail.com
Contraseña: 123456
